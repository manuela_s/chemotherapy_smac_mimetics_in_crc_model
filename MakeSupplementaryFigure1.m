function MakeSupplementaryFigure1()
%% Make Supplementary Figures 1.
% Function makes one set of panels for each cell_line (HCT116 and LoVo).
% For each cell line, the function plots substrate cleavage time-courses from
% simulations in SimulateEnsembleApoptoCellModel for the in_vitro and in_vivo
% experimental_model color-coded by treatment(5FU + Oxali, TL32711,
% 5FU + Oxali + TL32711, Control).

% Read simulation data
data = load('apoptocell_ensemble_simulations.mat', 'simulations');
simulations = data.simulations;
simulations.treatment = reordercats(simulations.treatment,...
    {'chemo', 'smac_mimetics', 'chemo_plus_smac_mimetics'});

% Make figure
figure();
p = matlab_utilities.panel.panel();

p.pack('h', 2);

MakeCellLinePlots(p(1), simulations, 'HCT116');
MakeCellLinePlots(p(2), simulations, 'LoVo');
ylabel(p, 'Substrate Cleavage [%]');

p.fontname = 'arial';
p.fontsize = 8;

p.margintop = 10;
p.children.margin = 10;

p.export(fullfile('figures', 'SupplementaryFigure1.pdf'),...
    '-rx', '-w186', '-h160');
end

function MakeCellLinePlots(p, simulations, cell_line)
%% Make panels for a cell line.
% Function plots one column for each experimental_model and
% one row for each treatment (5FU + Oxali, TL32711, 5FU + Oxali + TL32711).
% Each sub-panel includes a substrate cleavage timecourse and a histogram
% of substrate cleavage reached at the end of the simulations.
% Input arguments:
% - p: panel to plot in;
% - simulations:  table with simulation data, as returned by
%   SimulateEnsembleApoptoCellModel;
% - cell_line, string, name of the cell line.

% Subset data by cell_line
simulations = simulations(simulations.cell_line == cell_line, :);
simulations.cell_line = removecats(simulations.cell_line);

% Subset data for the in_vitro and in_vivo experimental_model
in_vitro = simulations(simulations.experimental_model == 'in_vitro', :);
in_vivo = simulations(simulations.experimental_model == 'in_vivo', :);

p.pack('h', 2);

MakeSubstrateCleavageTimeCoursePlot(p(1), in_vitro,...
    'make_histogram', true, 'enable_ylabel', false);
h_in_vitro = title(p(1), 'in vitro');
MakeSubstrateCleavageTimeCoursePlot(p(2), in_vivo,...
    'make_histogram', true, 'enable_ylabel', false);
h_in_vivo = title(p(2), 'in vivo');

% Add a blank line in the title so it does not overlap with titles for 
% experimental_model
ht = title(p, {cell_line, ''});
set([h_in_vitro, h_in_vivo, ht], 'color', 'k');

p.children.margin = 7;
end
