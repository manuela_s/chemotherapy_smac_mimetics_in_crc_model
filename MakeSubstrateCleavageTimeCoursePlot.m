function MakeSubstrateCleavageTimeCoursePlot(p, simulations, varargin)
%% Make rows of plots with substrate cleavage time-courses by treatment.
% Function plots substrate cleavage time-courses predicted by either
% SimulateEnsembleExtendedApoptoCellModel or SimulateEnsembleApoptoCellModel
% following treatment with chemo (5FU + Oxali), smac_mimetics (TL32711) and
% chemo_plus_smac_mimetics (5FU + Oxali + TL32711) for one cell line.
% Input arguments:
% - p: panel to plot in;
% - simulations:  table with simulation data, as returned by either
%   SimulateEnsembleExtendedApoptoCellModel or SimulateEnsembleApoptoCellModel.

ip = inputParser();
ip.addParameter('make_histogram', false, @islogical);
ip.addParameter('enable_ylabel', true, @islogical);
ip.parse(varargin{:});

treatments = categories(simulations.treatment);

p.pack('v', numel(treatments));

PlotHelper(p(1), simulations, 'chemo', 'g', '5FU + Oxali', ip.Results.make_histogram, false);
PlotHelper(p(2), simulations, 'smac_mimetics', 'b', 'TL32711', ip.Results.make_histogram, false);
PlotHelper(p(3), simulations, 'chemo_plus_smac_mimetics', 'r',...
    '5FU + Oxali + TL32711', ip.Results.make_histogram, true);

hx = xlabel(p, 'Time [h]');
set(hx, 'color', 'k');

if ip.Results.enable_ylabel
    hy = ylabel(p, 'Substrate Cleavage [%]');
    set(hy, 'color', 'k');
end

p.children.margin = 1;
end

function PlotHelper(p, simulations, treatment, color, treatment_title, make_histogram, xt_label)
%% Helper function to make single plot.
% Input arguments:
% - p: panel to plot in;
% - simulations: table with simulation data for the cell line;
% - treatment: string, name of treatment;
% - color: color to be used for line plot;
% - treatment_title: string, name of treatment formatted for title;
% - make_histogram: logical, make histogram of substrate_cleavage_end on
%   the side?
% - xt_label: logical, print xticklabels?

sub_table = simulations(simulations.treatment == treatment, :);

if make_histogram
    p.pack('h', {0.8, 0.2});
    p(2).select();
    MakeHistogram(sub_table, color, xt_label);
    p(1).select();
else
    p.select();
end
MakeTimeCourse(sub_table, color, xt_label);
text(0.01, 0.9, treatment_title, 'color', color, 'units', 'normalized',...
    'fontname', 'arial', 'fontsize', 8);
end

function MakeTimeCourse(sub_table, color, xt_label)
%% Helper function to make single substrate cleavage time course plot.
% Input arguments:
% - sub_table: table with simulation data for the cell line and treatment;
% - color: color to be used for line plot;
% - xt_label: logical, print xticklabels?

simulations_length = size(sub_table.traces, 2)-1;
xlims = [0 simulations_length];
if simulations_length > 60*24
    xticks = 0:60*12:simulations_length;
    % Downsample trace data to 1 point every 15 minutes for performance
    x = xlims(1):15:xlims(2);
    y = sub_table.traces(:, 1:15:end, 3);
else
    xticks = 0:60:simulations_length;
    x = xlims(1):xlims(2);
    y = sub_table.traces(:, :, 3);
end
xticklabels = xticks ./ 60;

for i = 1:height(sub_table)
    matlab_utilities.patchline(x, y(i, :)',...
        'linewidth', 1, 'edgecolor', color, 'linestyle', '-', 'edgealpha', 0.2);
end
xlim([xlims(1)-1 xlims(2)+1]);
ylim([-5 105]);
set(gca, 'XTick', xticks, 'XTickLabel', [], 'YTick', [0:20:100],...
    'box', 'on', 'linewidth', 1, 'xcolor', 'k', 'ycolor', 'k');
if xt_label
    set(gca, 'XTickLabel', xticklabels);
end
end

function MakeHistogram(sub_table, color, xt_label)
%% Helper function to make single histogram plot.
% Input arguments:
% - sub_table: table with simulation data for the cell line and treatment;
% - color: color to be used for histogram plot;
% - xt_label: logical, print xticklabels?

substrate_cleavage_end = sub_table.traces(:, end, 3);
histogram(substrate_cleavage_end, 0:20:100,...
    'Normalization', 'probability',...
    'Orientation', 'horizontal',...
    'FaceColor', color, 'FaceAlpha', 1);
ylim([-5 105]);
xlim([0 1]);
set(gca, 'XTick', [0 0.5 1], 'XTickLabel', [],...
    'XColor', 'k', 'YColor', 'k',...
    'Box', 'on', 'LineWidth', 1);
if xt_label
    % Skip labels at 0% and 50% because it collides with the xlabels from
    % the traces.
    set(gca, 'XTickLabel', {'', '', '100%'});
end

end
