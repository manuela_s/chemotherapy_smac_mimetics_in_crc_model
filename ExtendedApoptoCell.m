function [traces, header, all_traces] = ExtendedApoptoCell(conc, settings, onset, ramp_duration, substrate_cleavage_at_ramp_end)
%% Run simulations for the extended APOPTO-CELL model.
% The kinetic of substrate cleavage is modelled with a piece-wise function 
% including three distinct phases: 
% 1) pre-caspases activation (flat);
% 2) pre-MOMP signalling (simulated as a linear function, ramp);
% 3) rapid execution (core APOPTO-CELL).
% Input arguments:
% - conc: array of initial concentrations in uM for Procaspase-3, Procaspase9,
%   XIAP, Apaf1, SMAC and smac_mimetics; 
% - settings: struct with duration [min] and stepsize [min];
% - onset: scalar, time of onset; 
% - ramp_duration: scalar, duration of the ramp;
% - substrate_cleavage_at_ramp_end: scalar, substrate cleavage reached at the end of ramp; 
% Output:
% - traces: matrix with time-courses of key species. See header;
% - header: description of the columns in traces;
% - all_traces: matrix with time-courses of extended set of species.

% Sanity check inputs
assert(numel(conc) == 6);
assert(all(conc >= 0));
assert((onset + ramp_duration) <= settings.duration);
assert(onset >= 0);
assert(ramp_duration >= 0);
assert((substrate_cleavage_at_ramp_end >= 0) & (substrate_cleavage_at_ramp_end <= 1));

% Run until substrate_cleavage_at_ramp_end or we run out of time
onset_steps = floor(onset / settings.stepsize);
ramp_steps = floor(ramp_duration / settings.stepsize);
ramp = (0:(ramp_steps - 1)) .* ((substrate_cleavage_at_ramp_end / ramp_duration) .* settings.stepsize);
settings.duration = settings.duration - (ramp_steps + onset_steps) .* settings.stepsize;
conc(7) = 1 - substrate_cleavage_at_ramp_end;
if settings.duration > 0
    [traces, header, all_traces] = cohort_utilities.ApoptoCell(conc, settings);
else
    traces = zeros(1, 3);
    traces(1, 3) = 100 .* substrate_cleavage_at_ramp_end;
    header = {'Casp. 9 [uM]', 'Casp. 3 [uM]', 'cl. Substrate [%]'};
    all_traces = zeros(1, 21);
    all_traces(1, 19) = 1 - substrate_cleavage_at_ramp_end;
end

% Prepend flat and ramp piece-wise curves
traces = [zeros((onset_steps + ramp_steps), 3); traces];
all_traces = [zeros((onset_steps + ramp_steps), 21); all_traces];
traces((onset_steps + 1):(onset_steps + ramp_steps), 3) = ramp .* 100;
all_traces(1:(onset_steps + ramp_steps), 19) = [ones(1, onset_steps), 1 - ramp];
end
