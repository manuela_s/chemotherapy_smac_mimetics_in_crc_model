function MakeFigure3PanelsC_D()
%% Make Figure 3, panels C-D.
% Function plots key parameters measured by single-cell imaging following
% treatment with chemo (5FU + Oxali), smac_mimetics (TL32711) and 
% chemo_plus_smac_mimetics (5FU + Oxali + TL32711) in HCT116 (panel C) and
% LoVo (panel D) colorectal cancer cell lines.
% Panels, left to right:
% i)   Onset pre-MOMP ramp [h]: time at which the pre-MOMP ramp onset was 
%      detected (in hours);
% ii)  Slope of pre-MOMP ramp [% cleaved substrate/min]: %SC/ramp_duration, 
%      where:
%      - %SC: difference between the substrate cleavage reached at the start
%        of the rapid execution phase and at the onset of pre-MOMP ramp. 
%        Substrate cleavage at the onset of pre-MOMP ramp is assumed to be 0;
%      - ramp_duration: difference between ramp_end_time_min and ramp_onset_time_min;
% iii) MOMP threshold [% substrate cleaved at MOMP]: substrate cleavage
%      reached at the start of the rapid execution phase (in %);
% iv)  Onset of execution phase [h] in panel iv): time at which rapid onset
%      of the mitochondrial engagement was detected (in hours).

% Read in key parameters dataset
data = load('extended_apoptocell_ensemble_simulations.mat', 'exps');
exps = data.exps;

cells = categories(exps.cell_line);
figure();
p = matlab_utilities.panel.panel();
matlab_utilities.MaximizeFigure();
p.pack('v', numel(cells));
for c = 1:numel(cells)
    % Plot parameters for each cell line in a separate row of panels
    RowHelper(p(c), exps(exps.cell_line == cells{c}, :));
end
p.fontname = 'arial';
p.fontsize = 8;
p.margin = 15;

p.export(fullfile('figures', 'Figure3CD.pdf'), '-rx', '-h130', '-w180');
end

function RowHelper(p, exps)
%% Plot row of panels including key parameters measured by single-cell imaging.
% Input arguments:
% - p : panel to plot in;
% - exps: table with experimental data, as returned by ReadSingleCellData
%   in SimulateEnsembleExtendedApoptoCellModel.m.

% Pack 4 panels, one for each of the 4 key experimental parameters
p.pack('h', 4);

% Onset pre-MOMP ramp [h], panel i)
p(1).select();
PanelHelper(...
    exps,...
    'time_before_ramp',...
    'Onset pre-MOMP ramp [h]',...
    60,...
    'ramp_and_momp',...
    [0 70]);
% Slope of pre-MOMP ramp [%cleaved substrate/min], panel ii)
p(2).select();
PanelHelper(...
    exps,...
    'ramp_slope',...
    sprintf('Slope of pre-MOMP ramp\n[%% cleaved substrate/min]'),...
    1,...
    'ramp_and_momp',...
    [0 3.5]);
% MOMP threshold [% substrate cleaved at MOMP], panel iii)
p(3).select();
PanelHelper(...
    exps,...
    'substrate_cleavage_at_ramp_end',...
    sprintf('MOMP threshold\n[%% substrate cleaved at MOMP]'),...
    1,...
    'ramp_and_momp',...
    [0 100]);
% Onset of execution phase [h], panel iv)
p(4).select();
PanelHelper(...
    exps,...
    'time_before_MOMP',...
    'Onset of execution phase [h]',...
    60,...
    'momp_only',...
    [0 70]);
title(p, unique(categories(removecats(exps.cell_line))));
end

function PanelHelper(exps, exps_input_col, y_lab, norm, curve_type, y_lim)
%% Plot single panel for a given experimental parameter and cell line.
% Input arguments:
% - exps: table with experimental data;
% - exps_input_col: string, name of the column in exps table to plot;
% - y_lab: string, label for y axis;
% - norm: scalar, normalization factor. Time duration is expressed in
%   minutes in exps table, but parameters in panel i) and ii) are plotted
%   in hours to aid interpretability;
% - curve_type: categorical, ramp_and_momp or momp_only;
% - y_lim; vector, limits for y axis.

% Subset experimental results by curve_type:
% - ramp_and_momp: ramp (caspase-8 activation)-related parameters, panels i-iii;
% - momp_only: onset of execution phase, panel iv).

sub_exps = exps(exps.curve_type == curve_type, :);

sub_exps.treatment = reordercats(sub_exps.treatment,...
    {'chemo', 'smac_mimetics', 'chemo_plus_smac_mimetics'});
sub_exps.treatment = renamecats(sub_exps.treatment,...
    {'chemo', 'smac_mimetics', 'chemo_plus_smac_mimetics'},...
    {'C', 'SM', 'C+SM'});
% Append 1 phantom NaN value for each group, to force
% MakeCombinedBoxplotSpreadPlot to include empty groups
values = [sub_exps.(exps_input_col); nan(numel(categories(sub_exps.treatment)), 1)];
groups = [sub_exps.treatment; categories(sub_exps.treatment)];

matlab_utilities.MakeCombinedBoxplotSpreadPlot(...
    values ./ norm,...
    {groups},...
    'offset', 0.3,...
    'marker_size', 3);
if strcmp(exps_input_col, 'ramp_slope')
    matlab_utilities.ticklabelformat(gca, 'y', '%.1f');
else
    matlab_utilities.ticklabelformat(gca, 'y', '%.0f');
end

xlim([0.75 3.5]);
ylim(y_lim);
hy = ylabel(y_lab, 'interpreter', 'none');
set(hy, 'color', 'k');
set(gca, 'xcolor', 'k', 'ycolor', 'k');
end
