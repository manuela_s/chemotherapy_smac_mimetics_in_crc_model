function v = TumourVolume(t, Kd, V0, a, K, t1)
%% Compute tumour volume.
% Estimate volume of tumour at given time based on Benzekry S et al., PLOS
% Comp. Biol, 2014 and Simeoni M et al., Cancer Research, 2004
% Input arguments:
% - t:  time to evaluate tumour size at (may be an array of time points), [day];
% - Kd: drug efficacy coefficient (treatment-specific), [day^(-1)];
% - V0: tumour volume at t = 0 (tumour-specific), [mm^3];
% - a:  proliferation kinetic coefficient (cell-line specific), [day^(-1)];
% - K:  carrying capacity coefficient (asymptotic maximal volume, cell-line
%   specific), [(mm^3)];
% - t1: treatment start time, [day].

% if t < t1
%     dv/dt = a * V - a * V ^2 / K;
%     volume1 = (K * V0 * exp(a .* t)) ./ (K - V0 + V0 * exp(a .* t));
% else
%     V1 = (K * V0 * exp(a .* t1)) ./ (K - V0 + V0 * exp(a .* t1));
%     dv/dt = a * V - a * V ^2 / K - Kd * V;
%     volume2 = -(K * (Kd - a)) ./ (a - (exp((Kd - a) .* (t - t1)) .* (a * V1 - K * a + K * Kd)) ./ V1);
% end
% Define v as piece-wise function:
% v = heaviside(t1 - t) * volume1 + heaviside(t - t1) * volume2

v = -(K .* V0 .* exp(a .* t) .* (myheaviside(t - t1) - 1)) ./ (K - V0 + V0 .* exp(a .* t)) -...
    (K .* V0 .* myheaviside(t - t1) .* exp(Kd .* t1 - Kd .* t + a .* t) .* (Kd - a)) ./...
    (Kd .* V0 + K .* a - V0 .* a - K .* Kd + V0 .* a .* exp(Kd .* t1 - Kd .* t + a .* t) - Kd .* V0 .* exp(a .* t1));
end

function y = myheaviside(x)
%% Compute heaviside step function.
% Input argument:
% - x: array of values to apply heaviside on.

y = x > 0;
end
