function FitInVivoTumourVolume()
%% Fit model parameters to tumour volume measurements from cell line xenografts.
% Fit model for tumour volume to observations in HCT116 and LoVo
% xenografts, treated with chemo (5FU + Oxali), smac_mimetics (TL32711),
% chemo_plus_smac_mimetics (5FU + Oxali + TL32711) and control. Fittings is 
% performed separately for each cell line. We carried out a fitting to estimate
% the parameters and we bootstrapped a 1000 times the fitting routine to 
% determine confidence intervals. See TumourVolume.m for details of the
% tumour growth model. 
% Fitting results are stored in in_vivo_tumour_volume_fittings.mat

% Run fittings and bootstrap analysis for HCT116 xenografts
exp_hct = GetTumourVolumePerCellLineXenograft('HCT116');
[fitting_hct, exp_fit_hct] = RunCellLineFittings(exp_hct); %#ok<ASGLU>
boot_hct = RunBootstrapCellLineFittings(exp_hct, 1000);

% Run fittings and bootstrap analysis for LoVo xenografts
exp_lovo = GetTumourVolumePerCellLineXenograft('LoVo');
[fitting_lovo, exp_fit_lovo] = RunCellLineFittings(exp_lovo); %#ok<ASGLU>
boot_lovo = RunBootstrapCellLineFittings(exp_lovo, 1000);

% Concatenate results for the HCT116 and LoVo xenografts into a single
% table for the fittings and bootstrap results
fittings = matlab_utilities.ConcatenateTables(...
    {fitting_hct.treatments, fitting_lovo.treatments},...
    'new_column', 'cell_line', 'new_column_values', {'HCT116', 'LoVo'}); %#ok<NASGU>
boots = matlab_utilities.ConcatenateTables(...
    {boot_hct, boot_lovo},...
    'keep_columns', {'Kd'},...
    'new_column', 'cell_line', 'new_column_values', {'HCT116', 'LoVo'}); %#ok<NASGU>

% Save results
save('in_vivo_tumour_volume_fittings.mat');
end

function t = GetTumourVolumePerCellLineXenograft(cell_line)
%% Get measurements of tumour volume over time for a cell line xenograft.
% Read in from spreadsheet tumour volume measurements for a cell_line xenograft
% (HCT116 or LoVo) treated with chemo (5FU + Oxali), smac_mimetics (TL32711),
% chemo_plus_smac_mimetics (5FU + Oxali + TL32711) and control.
% See code_book_mice_xenografts_tumour_volume_data.md and Figure 6C for more details.
% Input argument:
% - cell_line: string, name of the cell line xenograft;
% Output:
% - t: MATLAB table with tumour volume measurements for the specified cell line.
%   Table has the same columns as the spreadsheet and also adds "tumour_idx" 
%   (numeric dense identifier for each tumour xenograft).

t = readtable(fullfile('data', 'mice_xenografts_tumour_volume_data.csv'));
t.cell_line = categorical(t.cell_line);
t.treatment = categorical(t.treatment);
t.treatment = reordercats(t.treatment,...
    {'5FU + Oxali'; 'TL32711'; '5FU + Oxali + TL32711'; 'Control'});
t.tumour_id = categorical(t.tumour_id);

t = t(t.cell_line == cell_line & ~isnan(t.tumour_volume), :);
% Assign unique numeric ids to each tumour
t.tumour_id = removecats(t.tumour_id);
t.tumour_idx = grp2idx(t.tumour_id);
end

function results = RunBootstrapCellLineFittings(t, n)
%% Bootstrap cell line fittings. 
% Run cell line fitting n times, each time on a set of tumours randomly
% selected from the original set. For each treatment, we select random
% tumours with replacement, matching the number of tumours in the original
% set.
% Input arguments:
% - t: table with tumour volume data. One row for every volume measurement 
%   per tumour as returned by GetTumourVolumePorCellLineXenograft;
% - n: number of bootstrap iterations.
% Output:
% - results: table with bootstrap results, one row for every bootstrap 
%   iteration and one column for each fitting parameter. V0 fitting parameters
%   for tumours that were not present in the random sample will be NaN.

treatments = categories(t.treatment);
rng(0);
pb = matlab_utilities.ProgressBar(n, sprintf('%s_fittings_bootstrap',...
    char(categories(removecats(t.cell_line)))));
for i = 1:n
    randomized_tumour_ids = cell(1, numel(treatments));
    for tr = 1:numel(treatments)
        sub_t = t(t.treatment == treatments{tr}, :);
        % Select the random tumours to include for treatment tr.
        randomized_tumour_ids{tr} = randsample(...
            categories(removecats(sub_t.tumour_id)),...
            numel(categories(removecats(sub_t.tumour_id))),...
            true);
    end
    % Subset tumour volume measurements to selected tumour ids.
    randomized_tumour_ids_table = table(categorical(vertcat(randomized_tumour_ids{:})),...
        'VariableNames', {'tumour_id'});
    boot = innerjoin(t, randomized_tumour_ids_table, 'keys', 'tumour_id');
    s_boot(i) = RunCellLineFittings(boot); %#ok<AGROW>
    pb.update();
end
clear pb
results = struct2table(rmfield(s_boot, 'treatments'));
end

function [s, t] = RunCellLineFittings(t)
%% Fit parameters to tumour volumes for xenografts of a given cell line.
% Fit formula for tumour volume (as described in TumourVolume.m) to
% experimental results to estimate drug effect K_D.
% Input argument:
% - t: table with tumour volumes, as returned by GetTumourVolumePerCellLineXenograft;
% Outputs:
% - s: struct with fitted parameters, including fields as returned by ParseX 
%   and "treatments" (table with treatment-specific parameter, K_d);
% - t: table, including columns from the input argument, "treatment_idx"
%   and fitted parameters.

treatment = {'5FU + Oxali'; 'TL32711'; '5FU + Oxali + TL32711'; 'Control'};
% treatment_idx is used as an index into the fitting parameter, and must
% have fixed order, with control idx = 4
treatment = categorical(treatment, treatment);
treatment_idx = grp2idx(treatment);
treatment_table = table(treatment, treatment_idx);

t = innerjoin(t, treatment_table, 'keys', 'treatment');
% See ParseX for interpretation of the elements in the x0 array
x0 = [0.2, 300, 0.01, 0.01, 0.01, 25 .* ones(1, numel(categories(t.tumour_id)))];
% Run least-square curve fitting (requires Optimization toolbox)
x = lsqcurvefit(@VolumeFromX, x0, t, t.tumour_volume, [], [],...
    optimoptions('lsqcurvefit', 'Display', 'off'));
s = ParseX(x);
% Some tumours may be not be included in the fitting (for bootstrap).
% Set V_0 to NaN for excluded tumours, since we have no data to fit to.
excluded_tumour_ids = setdiff(1:numel(categories(t.tumour_id)), t.tumour_idx);
s.V0(excluded_tumour_ids) = nan;
treatment_table.Kd = s.Kd(treatment_table.treatment_idx)';
s.treatments = treatment_table;
t.V0 = s.V0(t.tumour_idx)';
t.Kd = s.Kd(t.treatment_idx)';
t.a = repmat(s.a, height(t), 1);
t.K = repmat(s.K, height(t), 1);
end

function s = ParseX(x)
%% Parse parameter array.
% Convert parameter array x into parameter structure s.
% Input argument:
% - x: parameter array as used by lsqcurvefit in RunCellLineFittings;
% Output:
% - s: parameter structure. See TumourVolume.m for description of the fields.

s.a = x(1);
s.K = x(2);
% s.Kd is an array of Kds for each treatment, indexed by treatment_idx.
% Control (idx = 4, is our baseline, and has Kd fixed to 0).
s.Kd = [x(3:5), 0];
% s.V0 is an array of initial volumes at t = 0 for each tumour, indexed by
% tumour_idx.
s.V0 = x(6:end);
end

function v = VolumeFromX(x, xdata)
%% Compute tumour volume from parameters.
% Compute tumour volumes given fitting parameters x to compare to measured 
% tumour volumes.
% Input arguments:
% - x: parameter array as used by lsqcurvefit in RunCellLineFittings;
% - xdata: table with measured tumour volumes, including:
%   - days: day the tumour volume was measured (integer);
%   - treatment_start_day: day the treatment was first administered;
%   - treatment_idx: numeric dense identifier for the treatment administered
%     to the tumour;
%   - tumour_idx: numeric dense identifier for the tumour;
% Output:
% - v: vector of computed tumour volumes, one element for every row in xdata.

s = ParseX(x);
t = xdata.days;
t1 = xdata.treatment_start_day;
v = TumourVolume(t, s.Kd(xdata.treatment_idx)', s.V0(xdata.tumour_idx)', s.a, s.K, t1);
end
