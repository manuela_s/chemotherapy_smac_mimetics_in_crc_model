function out = iif(cond, a, b)
%% IIF implements a ternary operator.
% Based on https://stackoverflow.com/questions/14612196/is-there-a-matlab-conditional-if-operator-that-can-be-placed-inline-like-vbas-i/27671554#27671554
% Input arguments:
% - cond: logical or array of logicals, condition(s) to evaluate;
% - a: scalar, array or cell array with value(s) to return when condition(s) is/are true;
% - b: scalar, array or cell array with value(s) to return when condition(s) is/are false;
% Output:
% - out: out has the same size as cond and has the same type as a and b, or
%   cell array if a and b are of different types or char array.

% Use cell output for either char or mixed type input
if ischar(a) || ischar(b) || ~strcmp(class(a), class(b))
    a = makecell(a);
    b = makecell(b);
    out = cell(size(cond));
end

a = make_replicated(a, size(cond));
b = make_replicated(b, size(cond));

out(cond) = a(cond);
out(~cond) = b(~cond);
end

function a = makecell(a)
%% Convert to cell array.
% Input arguments:
% - a: elemnt to make into cell array;
% Output:
% - a: cell array with a.

if ~iscell(a)
    a = {a};
end
end

function a = make_replicated(a, s)
%% Replicate a s times, if a is not already an array.
% Input arguments:
% - a: item to replicate;
% - s: size of the array to replicate to;
% Output:
% - a: replicated item.

if numel(a) == 1
    a = repmat(a, s);
else
    assert(isequal(size(a), s),...
        sprintf('Size of value(s) (%s) and condition(s) (%s) do not match.',...
        mat2str(size(a)), mat2str(s)));
end
end
