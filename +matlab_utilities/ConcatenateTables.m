function tables = ConcatenateTables(tables, varargin)
%% Concatenates tables vertically and pads columns if missing.
% Only works for tables with strings, not categorical nor numerical.
% Input argument:
% - tables: cell array of tables to concatenate;
% Output:
% - tables: single table with rows from all input tables.

p = inputParser();
p.addParameter('keep_columns', {}, @iscell); %Only keep columns with matching names
% To add a new column that identifies which table a row came from, use
% "new_column" and "new_column_values"
p.addParameter('new_column', '', @ischar);
p.addParameter('new_column_values', {}, @iscell);
p.parse(varargin{:});

if isempty(p.Results.new_column) ~= isempty(p.Results.new_column_values)
   error('Need to specify both new_column and new_column_values or neither'); 
end
if ~isempty(p.Results.new_column_values) &&...
        numel(p.Results.new_column_values) ~= numel(tables)
   error('Need one value in new_column_values for each input table'); 
end

if isempty(p.Results.keep_columns)
    columns = {};
    for i = 1:numel(tables)
        columns = union(columns, tables{i}.Properties.VariableNames);
    end
else
    columns = p.Results.keep_columns;
end

for i = 1:numel(tables)
    % Remove extra columns
    tables{i} = tables{i}(:, intersect(tables{i}.Properties.VariableNames, columns));
    % Pad columns missing from this table
    missing_cols = setdiff(columns, tables{i}.Properties.VariableNames);
    for j = 1:numel(missing_cols)
        tables{i}.(missing_cols{j}) = {''};
    end
    % Add new column
    if ~isempty(p.Results.new_column)
        tables{i}.(p.Results.new_column) = categorical(...
            repmat(p.Results.new_column_values(i), height(tables{i}), 1));
    end
end
tables = vertcat(tables{:});
end
