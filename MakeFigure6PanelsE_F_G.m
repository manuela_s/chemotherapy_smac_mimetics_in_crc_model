function MakeFigure6PanelsE_F_G()
%% Make Figure 6, panels E-G.
% Function plots modelled tumour growth in mice xenografts and relationship
% between fitted drug effect and simulated cell death.
% Panels:
% - E/F: Modelled tumour growth broken down by treatments (5FU + Oxali, TL32711,
%   5FU + Oxali + TL32711, Control) and cell line xenograft (HCT116 and Lovo
%   in panels E and F, respectively), as calculated by FitInVivoTumourVolume.
%   Initial volume (volume at time t = 0, V_0) was set to the median fitted
%   initial volume across all the tumours and treatments for each cell line xenograft;
% - G: Relationship between the fitted drug effect (mean K_D value from
%   bootstrap) and the simulated cell death (percentage of simulations with
%   SC>25% at the end of the simulation time, 72 h).

% Read fitted tumour growth in mice xenografts, as generated by FitInVivoTumourVolume
data = load('in_vivo_tumour_volume_fittings.mat');
sim_drug_effect = GetDrugEffect();

% Make figure
figure();
p = matlab_utilities.panel.panel();
p.pack('h', 3);

MakeFittedTumourgrowthTimecoursePlot(...
    p(1),...
    data.fitting_hct,...
    data.exp_fit_hct,...
    data.boot_hct,...
    'HCT116');
MakeFittedTumourgrowthTimecoursePlot(...
    p(2),...
    data.fitting_lovo,...
    data.exp_fit_lovo,...
    data.boot_lovo,...
    'LoVo');

MakeKDCellDeathScatterPlot(...
    p(3),...
    data.fittings,...
    data.boots,...
    sim_drug_effect);

p.fontname = 'arial';
p.fontsize = 8;
p.marginbottom = 2;

p.export(fullfile('figures', 'Figure6EFG.pdf'), '-rx', '-w180', '-h90');
end

function drug_effect = GetDrugEffect()
%% Calculate drug effect per cell line and per treatment.
% Get the estimated drug effect from the simulations, by calculating the
% percent of simulations for each cell_line/treatment combination that
% reaches substrate cleavage > 25%.
% Output:
% - drug_effect: table with drug effects. One row per group.
%   Columns:
%   - cell_line: string, name of the cell line (HCT116 or LoVo);
%   - treatment: string, name of the treatment (5FU + Oxali, TL32711 or
%     5FU + Oxali + TL32711);
%   - GroupCount: scalar, number of simulations in the group;
%   - mean_sc_end_is_high_percent: scalar, percentage of simulations in the
%     group with SC>25% at the end of the simulation.

sim = load('extended_apoptocell_ensemble_simulations.mat');
simulations = sim.simulations;
simulations.treatment = reordercats(categorical(simulations.treatment),...
    {'chemo', 'smac_mimetics', 'chemo_plus_smac_mimetics'});
simulations.treatment = renamecats(simulations.treatment,...
    {'chemo',       'smac_mimetics', 'chemo_plus_smac_mimetics'},...
    {'5FU + Oxali', 'TL32711',       '5FU + Oxali + TL32711'});
simulations = simulations(simulations.experimental_model == 'in_vivo',...
    {'cell_line', 'treatment', 'curve_type', 'group_number', 'conc', 'traces'});
% Substrate cleavage is trace number 3. Consider the SC at the end of the
% simulation high if it exceeds 25%. Multiply with 100 to get percent.
simulations.sc_end_is_high_percent = (simulations.traces(:, end, 3) > 25) .* 100;
drug_effect = grpstats(simulations, {'cell_line', 'treatment'}, @mean,...
    'DataVars', 'sc_end_is_high_percent');
end

function MakeFittedTumourgrowthTimecoursePlot(p, fitting, exp_fit, boot, cell_line)
%% Plot modelled tumour growth time-courses for cell line.
% Plot 1 time-course for each treatment. Each time-course has initial tumour
% volume set to the median fitted initial tumour volume across all tumours
% and all treatments for the cell line xenografts.
% Input arguments:
% - p: panel to plot in;
% - fitting: struct with fitted parameters for the cell line xenografts, as
%   returned by RunCellLineFittings in FitInVivoTumourVolume.m;
% - exp_fit: table with tumour volume data and fitted parameters, as
%   returned by RunCellLineFittings in FitInVivoTumourVolume.m. One row per
%   tumour/days;
% - boot: table with bootstrap fitted parameters, one row for each bootstrap
%   iteration, as returned by RunBootstrapCellLineFittings in FitInVivoTumourVolume.m;
% - cell_line: string, name of cell line.

p.pack('v', {[], {15}, {15}});
p(1).select();

colors = {'g', 'b', 'r', 'k'};
days = 0 : 0.25 : 30;

hg = hggroup(); % For hiding elements from the legend
for tr = 1 : height(fitting.treatments)
    % Compute volume confidence interval (ci) based on bootstrap fittings
    ci = ComputeVolumeCI(boot, days, unique(exp_fit.treatment_start_day),...
        fitting.treatments.treatment_idx(tr), 'median');
    patch([days, flip(days)], [ci(2, :), flip(ci(1, :))], colors{tr},...
        'FaceAlpha', 0.1, 'EdgeColor', 'none', 'parent', hg);
    hold on;
    % Plot modelled tumour volume based on fitted parameters (not bootstrapped)
    plot(days,...
        TumourVolume(days, fitting.treatments.Kd(tr), median(exp_fit.V0),...
        fitting.a, fitting.K, unique(exp_fit.treatment_start_day)),...
        '-', 'color', colors{tr}, 'linewidth', 1,...
        'displayname', char(fitting.treatments.treatment(tr)));
    hold on;
end

hx = xlabel('Time after implant [day]');
hy = ylabel('Fitted tumour volume [mm^3]');

yl = ([0 800]);
set(gca, 'Xlim', [0 30], 'XTick', [0 : 3 : 30], 'Ylim', yl,...
    'box', 'on', 'linewidth', 1, 'xcolor', 'k', 'ycolor', 'k');

% Add indicator for treatment start day
line([unique(exp_fit.treatment_start_day) unique(exp_fit.treatment_start_day)],...
    yl, 'color', 'k', 'linewidth', 1, 'linestyle', '--', 'parent', hg);
text(unique(exp_fit.treatment_start_day), 0.05*yl(2), ' treatment start',...
    'VerticalAlignment', 'bottom',...
    'fontname', 'arial', 'fontsize', 8);

p(3).select(legend(p(1).object, 'show'));

ht = title(p, cell_line);

set([hx, hy, ht], 'color', 'k');
end

function MakeKDCellDeathScatterPlot(p, fitting, boot, sim_drug_effect)
%% Make scatter plot for fitted drug effect vs. simulated cell death.
% - x: mean fitted drug effect (K_D) from bootstrap with error bars
%   indicating standard deviation;
% - y: simulated cell death (SC>25%), in %;
% Input arguments:
% - p: panel to plot in;
% - fitting: struct with fitted parameters for the cell line xenografts, as
%   returned by RunCellLineFittings in FitInVivoTumourVolume.m;
% - boot: table with bootstrap fitted parameters, one row for each bootstrap
%   iteration, as returned by RunBootstrapCellLineFittings in FitInVivoTumourVolume.m;
% - sim_drug_effect: table with drug effects, as returned by GetDrugEffect.

p.pack('v', {[], {15}, {15}});

% Add treatment_idx to drug_effect:
drug_effect = innerjoin(sim_drug_effect, fitting,...
    'keys', {'cell_line', 'treatment'},...
    'RightVariables', 'treatment_idx');

% Compute mean and standard deviation K_D for every cell_line/treatment_idx:
boot_t = stack([boot(:, {'cell_line'}), array2table(boot.Kd)], 2 : 5,...
    'NewDataVariableName', 'boot_Kd', 'IndexVariableName', 'treatment_idx');
boot_t.treatment_idx = str2double(regexprep(cellstr(boot_t.treatment_idx), 'Var', ''));
boot_stats = grpstats(boot_t, {'cell_line', 'treatment_idx'}, {@mean, @std});

% Join drug_effect from simulations and bootstrap statistics:
drug_effect = innerjoin(drug_effect, boot_stats, 'keys', {'cell_line', 'treatment_idx'},...
    'RightVariables', {'mean_boot_Kd', 'std_boot_Kd'});

% Calculate coordinates for text labels in scatter plot
% Left of the middle of the plot. Names go on the right
drug_effect.on_left = drug_effect.mean_boot_Kd < 0.015;
drug_effect.text_x = drug_effect.mean_boot_Kd +...
    drug_effect.on_left .* drug_effect.std_boot_Kd - ...
    ~drug_effect.on_left .* drug_effect.std_boot_Kd;
drug_effect.text_y = drug_effect.mean_sc_end_is_high_percent;

% Make plot
p(1).select();
colors = {'k', [0.5 0.5 0.5]};
cell_lines = categories(drug_effect.cell_line);

hg = hggroup(); % For hiding elements from the legend
hold on;
for i = 1 : numel(cell_lines)
    sub_drug_effect = drug_effect(drug_effect.cell_line == cell_lines{i}, :);
    
    plot(sub_drug_effect.mean_boot_Kd, sub_drug_effect.mean_sc_end_is_high_percent,...
        's', 'markerfacecolor', colors{i}, 'markeredgecolor', colors{i}, 'markersize', 3,...
        'linewidth', 1, 'DisplayName', cell_lines{i});
    hold on;
    hx = matlab_utilities.herrorbar(sub_drug_effect.mean_boot_Kd,...
        sub_drug_effect.mean_sc_end_is_high_percent,...
        sub_drug_effect.std_boot_Kd, 's');
    set(hx, 'MarkerFaceColor', colors{i}, 'Color', colors{i}, 'MarkerSize', 3,...
        'linewidth', 1, 'parent', hg);
    th = text(sub_drug_effect.text_x, sub_drug_effect.text_y,...
        cellstr(sub_drug_effect.treatment),...
        'color', colors{i}, 'interpreter', 'none',...
        'fontname', 'arial', 'fontsize', 8);
    set(th, {'HorizontalAlignment'},...
        matlab_utilities.iif(sub_drug_effect.on_left, 'left', 'right'));
end

xlabel('Fitted drug effect (K_D) [day^{-1}]');
ylabel('Simulated cell death (SC>25%) [%]');
matlab_utilities.ticklabelformat(gca, 'x', '%.2f');

% Add linear fit
pol = polyfit(drug_effect.mean_boot_Kd, drug_effect.mean_sc_end_is_high_percent, 1);
hr = refline(pol);
set(hr, 'linewidth', 1, 'linestyle', '--', 'color', 'k', 'DisplayName', 'identity line');

set(gca, 'box', 'on', 'linewidth', 1, 'xcolor', 'k', 'ycolor', 'k');
ylim([-2 30]);
xlim([-0.03 0.06]);

% Add correlation statistics
p(2).select();
set(gca, 'xcolor', 'none', 'ycolor', 'none', 'color', 'none');
[rp, pp] = corr(drug_effect.mean_sc_end_is_high_percent,...
    drug_effect.mean_boot_Kd,...
    'type', 'Pearson');
[rs, ps] = corr(drug_effect.mean_sc_end_is_high_percent,...
    drug_effect.mean_boot_Kd,...
    'type', 'Spearman');
text(0, 0,...
    sprintf('Pearson \\rho = %.2f - p-value = %.2f\nSpearman \\rho = %.2f - p-value = %.2f',...
    rp, pp, rs, ps),...
    'fontname', 'arial', 'fontsize', 8,...
    'Units', 'normalized', 'VerticalAlignment', 'bottom');

% Add legend in separate panel
p(3).select(legend(p(1).object, 'show'));
end

function ci = ComputeVolumeCI(boot, days, treatment_start_day, treatment_idx, tumour_idx)
%% Compute CI (25th-75th percentiles) of tumour volume based on bootstrap.
% CI is calculated by computing tumour volume based on parameters from each
% bootstrap iteration and computing the 25th and 75th percentiles.
% Input arguments:
% - boot: table with one row for each bootstrap, as returned by
%   RunBootstrapCellLineFittings() in FitInVivoTumourGrowth.m;
% - days: array of time points to calculate volume at treatment_start_day;
% - treatment_idx: index for treatment into boot.Kd;
% - tumour_idx: index for tumour into boot.V0, special value 'median' to
%   use median V0 across all tumours in boot regardless of treatment.
% Output:
% - ci: matrix with 1 row for 25th percentile and 1 row for 75th
%   percentile, and 1 column for every day in days input argument.

if strcmp(tumour_idx, 'median')
    V0 = nanmedian(boot.V0, 2);
else
    V0 = boot.V0(:, tumour_idx);
end

% For each bootstrap iteration, calculate the fitted tumour volume for
% each day in days
for br = 1:height(boot)
    boot_volume(br, :) = TumourVolume(days, boot.Kd(br, treatment_idx),...
        V0(br), boot.a(br), boot.K(br), treatment_start_day); %#ok<AGROW>
end

% Calculate 25th and 75th percentiles of the volumes from the different
% bootstrap iterations
ci = prctile(boot_volume, [25, 75]);
end
