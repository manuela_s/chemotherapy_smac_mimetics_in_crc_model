Code book for wb_proteins_data.csv

* cell_line: cell line name (HCT116 or LoVo);
* experimental_model: type of experimental model (in_vitro or in_vivo);
* replica: experiment identifier;
* protein: protein name (apaf1, caspase3, caspase9, smac, xiap);
* conc: concentration (in \muM) measured by Western blotting. 

For more information, refer to the article.