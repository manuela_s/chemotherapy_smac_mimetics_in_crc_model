Code book for annexin_pi_cell_death_data.csv

* cell_line: cell line name (HCT116 or LoVo);
* treatment: treatment name (chemo, smac_mimetics, chemo_plus_smac_mimetics);
* cell_death_percent: cell death (in %). Expressed as the mean across 
  Annexin V/PI staining measurements, after blanking for cell death in control conditions. 

For more information, refer to the article.