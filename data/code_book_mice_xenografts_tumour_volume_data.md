Code book for mice_xenografts_tumour_volume_data.csv

* cell_line: cell line name (HCT116 or LoVo);
* treatment_start_day: day at which the treatment was started;
* treatment: treatment name (Control, 5FU + Oxali, TL32711, 5FU + Oxali + TL32711);
* mouse_id: mouse identifier with names of the form number_color;
* tumour_id: tumour identifier with names of the form mouse_id_left|right;
  Each mouse hosted 2 xenografts implanted in the left and right flanks;
* days: time points at which tumour volumes were measured at;
* tumour_volume: volume of the tumour in mm^{3}.

For more information, refer to the article.
