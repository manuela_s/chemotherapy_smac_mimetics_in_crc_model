Code book for single_cell_imaging_data.csv

* cell_line: cell line name (HCT116 or LoVo);
* treatment: treatment name (chemo, smac_mimetics, chemo_plus_smac_mimetics);
* has_ramp: ramp was detected (1) or not (0);
* has_MOMP: MOMP was detected (1) or not (0);
* treatment_addition_time_min: time at which the treatment was added in relation to recording start time;
* ramp_onset_time_min: time at which the pre-MOMP ramp onset was detected (in minutes);
* ramp_end_time_min: time at which pre-MOMP ramp end was detected (in minutes);
* MOMP_onset_time_min: time at which Mitochondrial Outer Membrane Permeabilization (MOMP) onset was detected (in minutes);
* MOMP_end_time_min: time at which MOMP end was detected (in minutes);
* substrate_cleavage_at_ramp_end: substrate cleavage [%] reached at the end of the ramp (0 if has_ramp=0).

For more information, refer to the article.

