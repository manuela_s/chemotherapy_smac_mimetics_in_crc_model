function MakeFigure6PanelsA_B()
%% Make Figure 6, panels A-B.
% Function plots simulated cell death over time. Cell death is considered
% to have occurred when substrate cleavage exceeds 25%. The plot shows the
% percentage of simulations where cell death occurred over time broken down by
% cell line and treatment (5FU + Oxali, TL32711, 5FU + Oxali + TL32711).
% Panels:
% - A: HCT116 cell line;
% - B: LoVo cell line.

% Read simulation data
data = load('extended_apoptocell_ensemble_simulations.mat', 'simulations');
simulations = data.simulations;
simulations.treatment = reordercats(simulations.treatment,...
    {'chemo', 'smac_mimetics', 'chemo_plus_smac_mimetics'});
simulations.treatment = renamecats(simulations.treatment,...
    {'chemo',       'smac_mimetics', 'chemo_plus_smac_mimetics'},...
    {'5FU + Oxali', 'TL32711',       '5FU + Oxali + TL32711'});
% Extract trace number 3, substrate cleavage
simulations.substrate_cleavage = simulations.traces(:, :, 3);
% For each experimental_model (in_vitro or in_vivo), cell_line (HCT116 or
% LoVo) and treatment (5FU + Oxali, TL32711, 5FU + Oxali + TL32711),
% calculate the percentage of simulations in which cell death is predicted
% to occur (SC>25%)
stats = grpstats(simulations, {'experimental_model', 'cell_line', 'treatment'},...
    @(x) 100 .* mean(x > 25), 'DataVars', 'substrate_cleavage');
% Subset simulation data for the in_vivo experimental_model
stats = stats(stats.experimental_model == 'in_vivo', :);
stats.experimental_model = removecats(stats.experimental_model);

% Make figure
figure();
p = matlab_utilities.panel.panel();
cell_lines = categories(stats.cell_line);

p.pack('v', {0.975, 0.05});

p(1).pack('h', numel(cell_lines));
MakePlotsHelper(p(1, 1), stats(stats.cell_line == 'HCT116', :));
MakePlotsHelper(p(1, 2), stats(stats.cell_line == 'LoVo', :));
% Make single shared legends for both panels
matlab_utilities.CombineLegends(p(2), num2cell(findobj(gcf, 'type', 'legend')),...
    'create_subpanels', false);

p.fontname = 'arial';
p.fontsize = 8;
p.margin = 20;

p.export(fullfile('figures', 'Figure6AB.pdf'), '-rx', '-w158', '-h110');
end

function MakePlotsHelper(p, t)
%% Helper function to make plot for a single cell line.
% Input arguments:
% - p: panel to plot in;
% - t: table with aggregated simulation results, 1 row per treatment.
%   Columns include:
%   - cell_line: string, name of cell line;
%   - treatment: string, name of treatment;
%   - Fun1_substrate_cleavage: % of simulations with SC>25%.

p.select();

time = (0 : (size(t.Fun1_substrate_cleavage, 2) - 1)) ./ 60;% in h

hp = plot(time, t.Fun1_substrate_cleavage');

set(hp, {'color'}, {'g'; 'b'; 'r'}, {'DisplayName'}, cellstr(t.treatment));
set(gca, 'XTick', [0 : 6 : max(time)], 'Xlim', [0 max(time)],...
    'YTick', 0 : 10 : 100, 'YLim', [0 100],...
    'box', 'on', 'linewidth', 1, 'xcolor', 'k', 'ycolor', 'k');
hx = xlabel(p, 'Time [h]');
hy = ylabel(p, 'Simulated cell death (SC>25%)[%]');
ht = title(p, categories(removecats(t.cell_line)));
legend('show');
set([hx, hy, ht], 'color', 'k');
end
