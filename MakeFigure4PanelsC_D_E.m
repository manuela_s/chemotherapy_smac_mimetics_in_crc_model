function MakeFigure4PanelsC_D_E()
%% Make Figure 4, panels C-E.
% Function plots simulation inputs and resulting substrate cleavage time-courses
% from simulations for the in_vitro experimental_model in SimulateEnsembleExtendedApoptoCellModel.
% Panels:
% - C: protein concentrations used as simulation inputs;
% - D/E: substrate cleavage time-courses for HCT116 (panel D) and LoVo
%  (panel E).

% Read simulation data
data = load('extended_apoptocell_ensemble_simulations.mat', 'simulations');
simulations = data.simulations;
simulations.treatment = reordercats(simulations.treatment,...
    {'chemo', 'smac_mimetics', 'chemo_plus_smac_mimetics'});
% Subset simulation data for the in_vitro experimental_model
in_vitro = simulations(simulations.experimental_model == 'in_vitro', :);

% Make figure
figure();
p = matlab_utilities.panel.panel();

p.pack('v', {0.2, 0.8});

MakeProteinConcentrationsPanels(p(1), in_vitro);

cell_lines = categories(in_vitro.cell_line);
p(2).pack('v', numel(cell_lines));
for row = 1 : numel(cell_lines)
    MakeSubstrateCleavageTimeCoursePlot(p(2, row),...
        in_vitro(in_vitro.cell_line == cell_lines{row}, :));
    title(p(2, row), cell_lines{row});
end

p.fontname = 'arial';
p.fontsize = 8;
p.margin = 10;
p(2).margin = 15;

p.export(fullfile('figures', 'Figure4CDE.pdf'), '-rx', '-w180', '-h210');
end
