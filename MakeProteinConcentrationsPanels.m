function MakeProteinConcentrationsPanels(p, simulations)
%% Make panels with histogram of protein concentrations for each cell line.
% The histograms show the protein concentrations used as simulation inputs,
% drawn from normal distributions based on the measured means and standard
% deviations from the experimental data (see Figure 4, panel B).
% Input arguments:
% - p: panel to plot in;
% - simulations: table with simulation data, as returned by
%   SimulateEnsembleExtendedApoptoCellModel.

p.pack('h', numel(categories(simulations.cell_line))+1);
hl{1} = PlotCellLineHelper(p(1), simulations(simulations.cell_line == 'HCT116', :));
hl{2} = PlotCellLineHelper(p(3), simulations(simulations.cell_line == 'LoVo', :));
matlab_utilities.CombineLegends(p(2), hl, 'create_subpanels', false);
end

function hl = PlotCellLineHelper(p, sub_table)
%% Make histograms for protein concencentrations for a single cell line.
% Input arguments:
% - p: panel to plot in;
% - sub_table: table with simulation data for the cell line;
% Output:
% - hl: legend handle.

p.select();
colors = {'c', 'k', 'g', 'y', 'm'};
protein_names = {'Procaspase-3', 'Procaspase-9', 'XIAP', 'Apaf-1', 'SMAC'};
for i = 1:numel(protein_names)
    histogram(sub_table.conc(:, i), logspace(-3, 0, 50), 'normalization', 'probability',...
        'FaceColor', colors{i}, 'FaceAlpha', 0.6, 'DisplayName', protein_names{i});
    hold on;
end
matlab_utilities.ticklabelformat(gca, 'y', '%.1f');
set(gca, 'Xscale', 'log', 'XTick', [1e-3, 1e-2, 1e-1, 1], 'XLim', [1e-3 1],...
    'YTick', [0:.1:1], 'YLim', [0 1], 'XGrid', 'on', 'box', 'on',...
    'xcolor', 'k', 'ycolor', 'k');
hx = xlabel('Protein Concentrations [\muM]');
hy = ylabel('Fraction of simulations');
xlim([1e-3 1]);
hl = legend('show', 'Location', 'northwest');
set(hl, 'fontname', 'arial', 'fontsize', 8);
ht = title(char(unique(sub_table.cell_line)));
set([hx, hy, ht], 'color', 'k');
end
