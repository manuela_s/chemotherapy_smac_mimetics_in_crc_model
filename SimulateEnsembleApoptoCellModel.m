function simulations = SimulateEnsembleApoptoCellModel()
%% Simulate the APOPTO-CELL model.
% It runs APOPTO-CELL while only varying protein concentrations and treatment
% conditions and disregarding changes in pre-MOMP caspase activation preceding
% the onset of mitochondrial engagement ('momp_only' simulations).
%
% Simulations are run for an ensemble of HCT116 and LoVo cells treated with
% 5FU + OXALI (chemo), TL32711 (smac_mimetics) and 5FU + OXALI + TL32711 
% (chemo_plus_smac_mimetics) for the in_vitro and in_vivo experimental models,
% resulting in 12000 'momp_only' simulations
% (1000 simulations x 2 cell_lines x 3 treatments x 2 experimental_models).
%
% Output:
% - simulations: table with simulation results, including the same fields
%   (columns) as sims passed in as input argument and traces. One row per
%   simulation.


settings.duration = 300; %[min]
settings.stepsize = 1;   %[min]

% Load 
sims = load('extended_apoptocell_ensemble_simulations.mat', 'simulations');
simulations = sims.simulations;
simulations = simulations(:, {'cell_line', 'treatment', 'experimental_model',...
    'conc'});

p = matlab_utilities.ProgressBar(height(simulations),...
    'apoptocell_ensemble_simulations',...
    'enable_gui', false);
parfor i = 1:height(simulations)
    traces(i, :, :) = cohort_utilities.ApoptoCell(...
        simulations{i, 'conc'}, settings); %#ok<PFBNS>
    p.update(); %#ok<PFBNS>
end
clear p i;
simulations.traces = traces;

save('apoptocell_ensemble_simulations.mat');
end
