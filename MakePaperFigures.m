function MakePaperFigures()

if exist('figures', 'dir') == 0
    mkdir('figures');
end

% Figure 3, panels C-D
MakeFigure3PanelsC_D();

% Figure 4, panels C-E
MakeFigure4PanelsC_D_E();

% Figure 5, panels D-E
MakeFigure5PanelsD_E();

% Figure 6
% Figure 6 A-B
MakeFigure6PanelsA_B();
% Figure 6 E-G
MakeFigure6PanelsE_F_G();

% Supplementary figures 1
MakeSupplementaryFigure1();

% Auxiliary figures 1-2 (per tumour fittings). These figures are not
% included in the paper.
MakeAuxiliaryFigures1_2();
end
