function simulations = SimulateEnsembleExtendedApoptoCellModel()
%% Simulate the extended APOPTO-CELL model.
% It models pre-MOMP caspase activation preceding the onset of mitochondrial
% engagement and links it to APOPTO-CELL. Simulations are run for an ensemble
% of HCT116 and LoVo cells treated with 5FU + OXALI (chemo), TL32711 (smac_mimetics)
% and 5FU + OXALI + TL32711 (chemo_plus_smac_mimetics) for the in_vitro and
% in_vivo experimental models, resulting in ~12000 simulations
% (1000 simulations x 2 cell_lines x 3 treatments x 2 experimental_models)

settings.duration = 72*60; %[min]
settings.stepsize = 1;   %[min]
exps = ReadSingleCellData();
simulation_groups = ComputeSimulationGroups(exps);
simulations = PickSimulationProperties(simulation_groups, settings, 1000);
simulations = RunEnsembleSimulations(simulations, settings);
save('extended_apoptocell_ensemble_simulations.mat');
end

function sims = RunEnsembleSimulations(sims, settings)
%% Run the extended APOPTO-CELL simulations.
% Input arguments:
% - sims: table with simulation inputs, including concentrations (conc) and
%   ramp-related parameters (onset, ramp_duration and substrate_cleavage_at_ramp_end).
%   One row per simulation, as returned by PickSimulationProperties;
% - settings: struct with fields duration (scalar, in min) and stepsize (scalar, in min).
%   See also cohort_utilities.ApoptoCell;
% Output:
% - sims: table with simulation results, including the same fields
%   (columns) as sims passed in as input argument and traces. One row per
%   simulation.

p = matlab_utilities.ProgressBar(height(sims),...
    'extended_apoptocell_ensemble_simulations',...
    'enable_gui', false);
parfor i = 1 : height(sims)
    traces(i, :, :) = ExtendedApoptoCell(...
        sims{i, 'conc'},...
        settings,...
        sims{i, 'onset'},...
        sims{i, 'ramp_duration'},...
        0.01 .* sims{i, 'substrate_cleavage_at_ramp_end'}); % Converts substrate cleavage from percentage to fraction
    p.update();
end
clear p i;
sims.traces = traces;
end

function simulations = PickSimulationProperties(simulation_groups, settings, num_simulations)
%% Prepare simulations to run.
% For every simulation group, decide number of simulation based on the
% simulation_percent given for the group in simulation_groups and
% num_simulations, so that we run num_simulations simulations for every
% combination of experimental_model, cell_line and treatment.
% Each of the simulations in the simulation_groups is run with random
% protein concentrations, time_before_ramp, ramp_slope and 
% substrate_cleavage_at_ramp_end from normal distributions given by the 
% simulation_groups.
% Input arguments:
% - simulation_group: table with characteristics of the simulation groups
%   as returned by ComputeSimulationGroups. One row for each combination of
%   cell_line (HCT116 or LoVo) x treatment (chemo, smac_mimetics or chemo_plus_smac_mimetics) x
%   curve_type (momp_only, ramp_and_momp or flat) x experimental_model (in_vitro or in vivo);
% - settings: struct with fields duration (scalar, in min) and stepsize (scalar, in min).
%   See also ExtendedApoptoCell and cohort_utilities.ApoptoCell;
% - num_simulations: scalar, number of simulations to run.
% Output:
% - simulations: table with simulation inputs for the extended APOPTO-CELL
%   model. Table has the same format as simulation_groups, but also includes:
%   - conc: array of initial concentrations in uM for Procaspase-3, Procaspase-9,
%     XIAP, Apaf1, SMAC and smac_mimetics;
%   - onset: scalar, time of onset;
%   - ramp_duration: scalar, duration of the ramp;
%   - substrate_cleavage_at_ramp_end: scalar, substrate cleavage reached at
%     the end of ramp.

% Set random seed
rng('default');
for i = 1 : height(simulation_groups)
    % The number of simulations for each group is estimated based on
    % relative frequency of flat/ramp + momp/momp_only from experimental
    % data, so rounding is needed to ensure an integer number of
    % simulations
    tmp{i} = repmat(...
        simulation_groups(i, :),...
        round(0.01*num_simulations*simulation_groups.simulation_percent(i)),...
        1); %#ok<AGROW>
end
simulations = vertcat(tmp{:});
% Set protein concentrations for simulations and ensure values are >= 0
simulations.conc = max(...
    [simulations.protein_means, simulations.smac_mimetics_conc_uM] +...
    [simulations.protein_stds, zeros(height(simulations), 1)] .* randn(height(simulations), 6),...
    0);
% Set ramp onset for simulations and ensure values are [0, settings.duration]
simulations.onset = max(...
    min(...
    simulations.mean_time_before_ramp + simulations.std_time_before_ramp .* randn(height(simulations), 1),...
    settings.duration),...
    0);
% Set ramp slope for simulations and ensure values are >= 0
slope = max(...
    simulations.mean_ramp_slope + simulations.std_ramp_slope .* randn(height(simulations), 1),...
    0);
% Set substrate_cleavage_at_ramp_end for simulations and ensure values are [0, 100]
simulations.substrate_cleavage_at_ramp_end = min(max(...
    simulations.mean_substrate_cleavage_at_ramp_end +...
    simulations.std_substrate_cleavage_at_ramp_end .* randn(height(simulations), 1), 0), 100);
% Calculate ramp_duration based on the random substrate_cleavage_at_ramp_end and slope.
% Ensure that ramp ends before settings.duration
simulations.ramp_duration = min((simulations.substrate_cleavage_at_ramp_end ./ slope),...
    (settings.duration - simulations.onset));
simulations.ramp_duration(~simulations.has_ramp) = 0;
end

function proteins_stats_w = GetCellLineProteins()
%% Read in from spreadsheet protein expression measured by Western blotting.
% See code_book_wb_proteins_data.md for a description of the fields
% and Figures 4B-C and 5A-C.
% Output:
% * proteins_stats_w: table with protein expression determined by Western blotting
%   for HCT116 and LoVo cell lines for both the in vitro and in vivo experimental_model.

proteins = readtable(fullfile('data', 'wb_proteins_data.csv'));
proteins.cell_line = categorical(proteins.cell_line);
proteins.experimental_model = categorical(proteins.experimental_model);
proteins_stats = grpstats(...
    proteins,...
    {'cell_line', 'experimental_model', 'protein'},...
    {@mean, @std},...
    'DataVars', 'conc');
proteins_stats.GroupCount = [];
proteins_stats_w = unstack(proteins_stats, {'mean_conc', 'std_conc'},...
    'protein', 'GroupingVariables', {'cell_line', 'experimental_model'});
proteins_stats_w.protein_means = proteins_stats_w{:, {'mean_conc_caspase3',...
    'mean_conc_caspase9', 'mean_conc_xiap', 'mean_conc_apaf1', 'mean_conc_smac'}};
proteins_stats_w.protein_stds = proteins_stats_w{:, {'std_conc_caspase3',...
    'std_conc_caspase9', 'std_conc_xiap', 'std_conc_apaf1', 'std_conc_smac'}};
proteins_stats_w = proteins_stats_w(:, {'cell_line', 'experimental_model',...
    'protein_means', 'protein_stds'});
proteins_stats_w.Properties.RowNames = {};
end

function simulation_groups = ComputeSimulationGroups(exps)
%% Compute characteristics of each simulation group.
% For each combination of experimental_model (in_vitro, in_vivo), cell_line
% (HCT116, LoVo) and treatment (chemo, smac_mimetics, chemo_plus_smac_mimetics),
% we calculate the relative frequency of curve_types (flat, momp_only,
% ramp_and_momp) based on the ratio of surviving (flat) and apoptotic
% (momp_only and ramp_and_momp) cells from Annexin V/propidium iodide staining
% experiments, and the ratio of momp_only vs ramp_and_momp cells from single cell
% microscopy experiments.
% Input argument:
% - exps: table with experimental data from single cell imaging as returned
%   by ReadSingleCellData();
% Output:
% - simulation_groups: table with characteristics for each simulation group.
%   One row for each group. A group is defined as the
%   combination of cell_line, treatment, curve_type, experimental_model.
%   Columns include:
%   - cell_line: string, name of cell line (HCT116 or LoVo);
%   - treatment: string, name of treatment (chemo, smac_mimetics, or
%     chemo_plus_smac_mimetics);
%   - curve_type: string, name of curve type (flat, momp_only or
%     ramp_and_momp);
%   - has_ramp: logical, whether ramp occurred (1) or not (0);
%   - has_MOMP: logical, whether MOMP occurred (1) or not (0);
%   - mean_time_before_ramp: mean of time_before_ramp, as returned
%     by GetCellDeathData. Time_before_ramp is the time from treatment addition
%     to onset of ramp. For groups without a ramp, it is the time from treatment
%     addition to MOMP onset. For flat curve types, it is set to Inf;
%   - std_time_before_ramp: standard deviation of time_before_ramp,
%     as returned by GetCellDeathData. For flat curve types, it is set to 0;
%   - mean_ramp_slope: mean of ramp_slope as returned by GetCellDeathData.
%     For flat and momp_only curve types, it is set to 0;
%   - std_ramp_slope: standard deviation of ramp_slope, as returned
%     by GetCellDeathData. For flat and momp_only curve types, it is set to 0;
%   - mean_substrate_cleavage_at_ramp_end: mean of substrate_cleavage_at_ramp_end,
%     as returned by GetCellDeathData.For flat and momp_only curve types, it is set to 0;
%   - std_substrate_cleavage_at_ramp_end: standard deviation of
%     substrate_cleavage_at_ramp_end, as returned by GetCellDeathData.
%     For flat and momp_only curve types, it is set to 0;
%   - simulation_percent: percentage of simulations for a given simulation
%     group. Percentage of simulations for each cell_line, treatment and
%     experimental_model combination amounts to 100%;
%   - experimental_model: string, type of experiment (in_vitro or in_vivo);
%   - protein_means: array, mean protein concentrations, as returned by
%     GetCellLineProteins;
%   - protein_stds: array, standard deviations of protein concentrations,
%     as returned by GetCellLineProteins;
%   - smac_mimetics_conc_uM: scalar, concentration of SMAC_mimetic;
%   - group_number: array of ids to identify simulation_group.

cell_death = GetCellDeathData();
num_cells = grpstats(exps, {'cell_line', 'treatment'}, {}, 'DataVars', false);
cell_death = innerjoin(cell_death, num_cells);

grouped = grpstats(exps, ...
    {'cell_line', 'treatment', 'curve_type', 'has_ramp', 'has_MOMP'},...
    {'mean', 'std'},...
    'DataVars', {'time_before_ramp', 'ramp_slope', 'substrate_cleavage_at_ramp_end'});

% Construct simulation groups "grouped" from single-cell imaging experiments
% with curve_types:
% - momp_only: rapid onset of the mitochondrial engagement;
% - ramp_and_momp: upstream caspase-8 activation prior to MOMP followed by
% mitochondrial engagement.
grouped = innerjoin(grouped, cell_death, 'Keys', {'cell_line', 'treatment'});
grouped.simulation_percent = grouped.cell_death_percent .*...
    grouped.GroupCount_grouped ./...
    grouped.GroupCount_cell_death;
grouped.GroupCount_grouped = [];
grouped.cell_death_percent = [];
grouped.GroupCount_cell_death = [];

% Construct simulation groups "flat" with set parameters for flat curve_type.
% Set simulation_percent based on experimental cell death data, as returned
% by GetCellDeathData.
flat = cell_death;
flat.curve_type = categorical(repmat({'flat'}, height(cell_death), 1));
flat.simulation_percent = (100 - flat.cell_death_percent);
flat.cell_death_percent = [];
flat.GroupCount = [];
% Surviving cells (curve_type = flat) do not undergo neither pre-MOMP signalling
% nor rapid execution, thus:
% - time_before_ramp --> inf;
% - ramp_slope = 0;
% - substrate_cleavage_at_ramp_end = 0;
% - has_ramp = 0;
% - has_MOMP = 0
flat.mean_time_before_ramp = inf(height(flat), 1);
flat.std_time_before_ramp = zeros(height(flat), 1);
flat.mean_ramp_slope = zeros(height(flat), 1);
flat.std_ramp_slope = zeros(height(flat), 1);
flat.mean_substrate_cleavage_at_ramp_end = zeros(height(flat), 1);
flat.std_substrate_cleavage_at_ramp_end = zeros(height(flat), 1);
flat.has_ramp = false(height(flat), 1);
flat.has_MOMP = false(height(flat), 1);

% Get protein concentrations per cell_line and experimental_model
proteins = GetCellLineProteins();
% Get SMAC_mimetics concentrations per treatment
smac_mimetics_table = table(...
    categorical({'chemo'; 'smac_mimetics'; 'chemo_plus_smac_mimetics'}),...
    [0; 1; 1],...
    'VariableNames', {'treatment', 'smac_mimetics_conc_uM'});

simulation_groups = outerjoin([grouped; flat], proteins, 'MergeKeys', true);
simulation_groups = outerjoin(simulation_groups, smac_mimetics_table, 'MergeKeys', true);
simulation_groups.group_number = (1:height(simulation_groups))';
end

function cell_death = GetCellDeathData()
%% Read in from spreadsheet cell death data measured by Annexin/PI staining.
% See code_book_annexin_pi_cell_death_data.md for a description of each
% field and Figure 1C-D.
% Output:
% - cell_death: table with experimental results derived from Annexin/PI
%   staining for HCT116 and LoVo cell lines, treated with chemo, smac_mimetics
%   and chemo_plus_smac_mimetics both in vitro.

cell_death = readtable(fullfile('data', 'annexin_pi_cell_death_data.csv'));
cell_death.cell_line = categorical(cell_death.cell_line);
cell_death.treatment = categorical(cell_death.treatment);
end

function exps = ReadSingleCellData()
%% Read in from spreadsheet key parameters measured by single cell imaging.
% See code_book_single_cell_imaging_data.md for a description of the fields
% and Figure 3C-D.
% Output:
% - exps: table with experimental results derived from single cell imaging
%   for HCT116 and LoVo cell lines, treated with chemo, smac_mimetics and
%   chemo_plus_smac_mimetics both in vivo and in vitro.

exps = readtable(fullfile('data', 'single_cell_imaging_data.csv'));

exps.cell_line = categorical(exps.cell_line);
exps.treatment = categorical(exps.treatment);
exps.has_ramp = logical(exps.has_ramp);
exps.has_MOMP = logical(exps.has_MOMP);

exps.curve_type = categorical(repmat({''}, height(exps), 1));
exps.curve_type(~exps.has_ramp & exps.has_MOMP) = 'momp_only';
exps.curve_type(exps.has_ramp & exps.has_MOMP) = 'ramp_and_momp';

exps.time_before_ramp = exps.ramp_onset_time_min - exps.treatment_addition_time_min;
exps.time_before_MOMP = exps.MOMP_onset_time_min - exps.treatment_addition_time_min;
exps.ramp_duration = exps.ramp_end_time_min - exps.ramp_onset_time_min;
% Calculate ramp_slope as substrate cleavage % per min of ramp, or 0 for
% cells without ramp
exps.ramp_slope = exps.substrate_cleavage_at_ramp_end ./ exps.ramp_duration;
exps.ramp_slope(~exps.has_ramp) = 0;
end
